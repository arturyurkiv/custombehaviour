﻿using UnityEngine;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine.SceneManagement;

[InitializeOnLoad]
public class CastomScene 
{
    static CastomScene()
    {
       EditorSceneManager.newSceneCreated +=  SceneCreating;
    }

    public static void SceneCreating(Scene scene, NewSceneSetup setup, NewSceneMode mode)
    {
        var cam = Camera.main.transform;
        var light = GameObject.Find("Directional Light").transform;

        var setapFolder = new GameObject("[Setap]").transform;
        var lightFolder = new GameObject("Lights").transform;
        
        var cameraFolder = new GameObject("Camera").transform;
        light.parent = lightFolder;
        cam.parent = cameraFolder;


        cam.parent = cameraFolder;
        light.parent = lightFolder;

        lightFolder.parent = setapFolder;
        cameraFolder.parent = setapFolder;

       

        var world = new GameObject("[Word]").transform;

        new GameObject("Static").transform.parent = world;
        new GameObject("Dynemic").transform.parent = world;

        new GameObject("[UI]");

        setapFolder.gameObject.AddComponent<UpdateManager>();



        Debug.Log("Scene created");



    }



}
